#include <iostream>
#include <stdlib.h>
#include <ctime>
#include <mpi.h>
 
using namespace std;
 
int main(int argc, char * argv[]) {
 
    int numeroProcesadores,
            idProceso;
    int **A; // Matriz 
    int *x;// Vector 
    int *y; // Vector de resultado
    int *miFila; // La fila que almacena localmente un proceso
    int *comprueba; // Resultado final
 

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numeroProcesadores);
    MPI_Comm_rank(MPI_COMM_WORLD, &idProceso);
 
    A = new int *[numeroProcesadores]; 
    x = new int [numeroProcesadores]; 
 
    if (idProceso == 0) {
        A[0] = new int [numeroProcesadores * numeroProcesadores];
        for (unsigned int i = 1; i < numeroProcesadores; i++) {
            A[i] = A[i - 1] + numeroProcesadores;
        }
        y = new int [numeroProcesadores];
 
        srand(time(0));
        cout << "Matriz y vector aleatorios" << endl;
        for (unsigned int i = 0; i < numeroProcesadores; i++) {
            for (unsigned int j = 0; j < numeroProcesadores; j++) {
                if (j == 0) cout << "[";
                A[i][j] = rand() % 1000;
                cout << A[i][j];
                if (j == numeroProcesadores - 1) cout << "]";
                else cout << "  ";
            }
            x[i] = rand() % 100;
            cout << "\t  [" << x[i] << "]" << endl;
        }
        cout << "\n";
 
        comprueba = new int [numeroProcesadores];

        for (unsigned int i = 0; i < numeroProcesadores; i++) {
            comprueba[i] = 0;
            for (unsigned int j = 0; j < numeroProcesadores; j++) {
                comprueba[i] += A[i][j] * x[j];
            }
        }
    } 
 
    miFila = new int [numeroProcesadores];
 
   
    MPI_Scatter(A[0], numeroProcesadores, MPI_INT, miFila,  numeroProcesadores,  MPI_INT,  0,  MPI_COMM_WORLD);
    MPI_Bcast(x, numeroProcesadores,  MPI_INT, 0, MPI_COMM_WORLD);
 
    int subFinal = 0;
    for (unsigned int i = 0; i < numeroProcesadores; i++) {
        subFinal += miFila[i] * x[i];
    }

    MPI_Gather(&subFinal, 1,  MPI_INT, y,  1, MPI_INT, 0, MPI_COMM_WORLD); 
    MPI_Finalize();
 
    if (idProceso == 0) {
 
        unsigned int errores = 0;
 
        cout << "Resultado:" << endl;
        for (unsigned int i = 0; i < numeroProcesadores; i++) {
            cout << "\t" << y[i] << "\t|\t" << comprueba[i] << endl;
            if (comprueba[i] != y[i])
                errores++;
        }
    } 
}