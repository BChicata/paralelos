#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"	
#include <math.h>	

int main(int argc, char* argv[])
{
    long iteraciones = 1000000;		
    int rank;						
    int size;						
    double x;
	double y;
    int i;
	int contador;
    double pi;
	contador = 0;
 
    MPI_Init(&argc, &argv);		
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int array_recibidos[size];		
    long array_iteraciones[size];	
    srand(time(NULL));
 
    if(rank != 0)				
    {
        for (i=0; i<iteraciones; i++)
        {
            x= ((double)rand())/RAND_MAX;		
            y =((double)rand())/RAND_MAX;		

            if ((pow(x,2) + pow(y,2)) <= 1.0)   
            {
                contador++;
            }
        }
        for(i=0; i<size; i++)
        {
            MPI_Send(&contador, 1, MPI_INT, 0, rank, MPI_COMM_WORLD);
            MPI_Send(&iteraciones, 1, MPI_LONG, 0, rank, MPI_COMM_WORLD);
        }
    }
    else 
	{
		if (rank == 0)			
		{
		    for(i=0; i<size; i++)
		    {
		        MPI_Recv(&array_recibidos[i], size, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		        MPI_Recv(&array_iteraciones[i], size, MPI_LONG, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		    }
		}
	}
 
    if (rank == 0)		
    {
        int contador_final = 0;
        long val_iter_final = 0;
        for(i = 0; i<size; ++i)
        {
            contador_final += array_recibidos[i];
            val_iter_final += array_iteraciones[i];
        }
 
        pi = ((double)contador_final/(double)val_iter_final)*4.0;
        printf("valor pi - mpi -> %f\n", pi);
 
    }
 
    MPI_Finalize();				
    return 0;
}