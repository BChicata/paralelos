#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>	
#include <math.h>	

int thread_count;
int n ; 
int a ;
int b ; 
int h ; 
int local_n;

double funcion ( double x ) {
	double res = x*x;
	return res;
} 

double Trap(double a, double b, int n , int h ){
	double integral;
	double x;
	int i ;

	integral = (funcion(a) + funcion(b))/2;
	x = a;
	
	for ( i = 0 ; i < n  ; i++) {
		x= a + i*h;
		integral += funcion(x);
	}
	integral = integral*h;
	return integral;
}

void* Trapezoidal (void* rank) {
	long my_rank = (long) rank;
	double local_a = a + my_rank*local_n;
	double local_b = local_a + local_n*h;
	
	Trap( local_a, local_b , local_n, h);
	return NULL;
}



int main ( int argc , char* argv[] ) {
	printf ("Ingrese a, b y n ");
	scanf("%d %d %i", &a , &b , &n );
	h = (b-a)/n ;
	local_n = n/thread_count; 

	long thread ;
	pthread_t* thread_handles;

	//Obtener el numero d threads 
	thread_count = strtol ( argv[1] , NULL , 10);

	thread_handles = malloc ( thread_count*sizeof(pthread_t));

	for ( thread = 0 ; thread<thread_count ; thread++) {
		pthread_create(&thread_handles[thread], NULL, Trapezoidal, (void*) thread);
	}
	
	for (thread = 0 ; thread<thread_count ; thread++) {
		pthread_join ( thread_handles[thread],NULL);
	}
	
	//printf (resultado);
	free ( thread_handles);
	return 0 ;

}